#include <iostream>
#include <fstream>
#include <sstream>
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <lcm/lcm-cpp.hpp>

#include "exlcm/example_t.hpp"

#define PI 3.1415926
using namespace std;

geometry_msgs::PoseStamped uav_cur_pose;
geometry_msgs::TwistStamped uav_cur_vel;
int droneID = 0;
std::string uav_name = "/uav" + to_string(droneID);
int ctrl_rate = 30;

void uav_pose_cb(const geometry_msgs::PoseStamped::ConstPtr& msg){
    uav_cur_pose = *msg;
}

void uav_vel_cb(const geometry_msgs::TwistStamped::ConstPtr& msg){
    uav_cur_vel = *msg;
}

// Main func
int main(int argc, char** argv){
    
    // ros init
    ros::init(argc, argv, "ros_lcm", ros::init_options::AnonymousName);
    ros::NodeHandle nh;

    lcm::LCM lcm("udpm://239.255.76.67:7667?ttl=1");
    if(!lcm.good())
        return 1;

    ros::Time last_request = ros::Time::now();
    
    ros::Subscriber uav_pose_sub = nh.subscribe<geometry_msgs::PoseStamped>
            (uav_name + "/mavros/local_position/pose", 2, uav_pose_cb);
    ros::Subscriber uav_vel_sub = nh.subscribe<geometry_msgs::TwistStamped>
            (uav_name + "/mavros/local_position/velocity_local", 1, uav_vel_cb);
    
    ros::Rate ctrl_loop(ctrl_rate);
    
    //main ctrl loop
    while(ros::ok()) {

        exlcm::example_t my_data;
        my_data.timestamp = ros::Time::now().toSec();
        my_data.position[0] = uav_cur_pose.pose.position.x;
        my_data.position[1] = uav_cur_pose.pose.position.y;
        my_data.position[2] = uav_cur_pose.pose.position.z;
        my_data.orientation[0] = uav_cur_pose.pose.orientation.w;
        my_data.orientation[1] = uav_cur_pose.pose.orientation.x;
        my_data.orientation[2] = uav_cur_pose.pose.orientation.y;
        my_data.orientation[3] = uav_cur_pose.pose.orientation.z;
        my_data.num_ranges = 15;
        my_data.ranges.resize(my_data.num_ranges);
        for(int i = 0; i < my_data.num_ranges; i++)
            my_data.ranges[i] = i;
        my_data.name = "example string from computer1";
        my_data.enable = true;
        lcm.publish("EXAMPLE", &my_data);
        
        ros::spinOnce();
        ctrl_loop.sleep();
    }
    return 0;
}