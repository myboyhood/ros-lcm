#include <iostream>
#include <fstream>
#include <sstream>
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <lcm/lcm-cpp.hpp>

#include "exlcm/example_t.hpp"

#define PI 3.1415926
using namespace std;

geometry_msgs::PoseStamped uav_cur_pose;
geometry_msgs::TwistStamped uav_cur_vel;
int droneID = 0;
std::string uav_name = "/uav" + to_string(droneID);
int ctrl_rate = 30;

class Handler 
{
    public:
        ~Handler() {}
        void handleMessage(const lcm::ReceiveBuffer* rbuf,
                const std::string& chan, 
                const exlcm::example_t* msg)
        {
            int i;
            printf("Received message on channel \"%s\":\n", chan.c_str());
            printf("  timestamp   = %lld\n", (long long)msg->timestamp);
            printf("  position    = (%f, %f, %f)\n",
                    msg->position[0], msg->position[1], msg->position[2]);
            printf("  orientation = (%f, %f, %f, %f)\n",
                    msg->orientation[0], msg->orientation[1], 
                    msg->orientation[2], msg->orientation[3]);
            printf("  ranges:");
            for(i = 0; i < msg->num_ranges; i++)
                printf(" %d", msg->ranges[i]);
            printf("\n");
            printf("  name       = '%s'\n", msg->name.c_str());
            printf("  enable     = %d\n", msg->enable);
        }
};

// Main func
int main(int argc, char** argv){
    
    // ros init
    ros::init(argc, argv, "ros_lcm", ros::init_options::AnonymousName);
    ros::NodeHandle nh;

    lcm::LCM lcm("udpm://239.255.76.67:7667?ttl=0");

    Handler handlerObject;
    lcm.subscribe("EXAMPLE", &Handler::handleMessage, &handlerObject);

    ros::Rate ctrl_loop(ctrl_rate);
    
    //main ctrl loop
    while(ros::ok()) {

        ros::spinOnce();
        if (!lcm.good()) {
            printf("stop\n");
            break;
        }
        if (lcm.handle() != 0)
            break;
        ctrl_loop.sleep();
    }
    return 0;
}