#include <iostream>
#include <fstream>
#include <sstream>
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <lcm/lcm-cpp.hpp>
#include <colors.h>
#include <lcm_geometry_msgs/PoseStamped.hpp>
#include <lcm_geometry_msgs/PositionStamped.hpp>
#include <lcm_geometry_msgs/TwistStamped.hpp>

#define PI 3.1415926
using namespace std;

geometry_msgs::PoseStamped uav_cur_pose;
geometry_msgs::TwistStamped uav_cur_vel;
int droneID = 0;
std::string uav_name = "/uav" + to_string(droneID);
std::string target_uav_name = "kun1";
std::string uav_vel_msg, uav_pose_msg;
int ctrl_rate = 30;
double target_pos_offset[3] = {0, -1, 0}; //在kun0正常当前值上加上这个偏移量，得到kun1的目标值

// tool func
template<typename T>
void readParam(ros::NodeHandle &nh, std::string param_name, T& loaded_param) {
    // template to read param from roslaunch
    const string& node_name = ros::this_node::getName();
    param_name = node_name + "/" + param_name;
    if (!nh.getParam(param_name, loaded_param)) 
        ROS_ERROR_STREAM("Failed to load " << param_name << ", use default value");
    else
        ROS_INFO_STREAM("Load " << param_name << " success");
}
void loadRosParams(ros::NodeHandle &nh) {
    readParam<int>(nh, "droneID", droneID);
    ROS_INFO_STREAM("drone ID = " << droneID);
    uav_name = "/uav" + to_string(droneID);
    uav_pose_msg = uav_name + "/mavros/local_position/pose";
    uav_vel_msg = uav_name + "/mavros/local_position/velocity_local";
}

void uav_pose_cb(const geometry_msgs::PoseStamped::ConstPtr& msg){
    uav_cur_pose = *msg;
}

void uav_vel_cb(const geometry_msgs::TwistStamped::ConstPtr& msg){
    uav_cur_vel = *msg;
}

// Main func
int main(int argc, char** argv){
    
    // ros init
    ros::init(argc, argv, "ros_lcm_pub", ros::init_options::AnonymousName);
    ros::NodeHandle nh;

    // load param
    loadRosParams(nh);

    lcm::LCM lcm("udpm://239.255.76.67:7667?ttl=1");
    if(!lcm.good())
        return 1;

    ros::Time last_request = ros::Time::now();
    ros::Subscriber uav_pose_sub = nh.subscribe<geometry_msgs::PoseStamped>
            (uav_name + "/mavros/local_position/pose", 2, uav_pose_cb);
//    ros::Subscriber uav_vel_sub = nh.subscribe<geometry_msgs::TwistStamped>
//            (uav_name + "/mavros/local_position/velocity_local", 1, uav_vel_cb);
    
    ros::Rate ctrl_loop(ctrl_rate);
    
    //main ctrl loop
    while(ros::ok()) {
      ros::spinOnce();

      lcm_geometry_msgs::PositionStamped pos;
      pos.timestamp = ros::Time::now().toSec();
      pos.name = "kun0";
      if(abs(uav_cur_pose.pose.position.x) > 1e-7 && abs(uav_cur_pose.pose.position.y) > 1e-7){
        pos.enabled = true;
        printf(GREEN "[Normal] uav_cur_pose: x = %.3f, y = %.3f, z = %.3f\n", uav_cur_pose.pose.position.x, uav_cur_pose.pose.position.y, uav_cur_pose.pose.position.z);
      }else{
        printf(RED "[Bad] uav_cur_pose: x = %.3f, y = %.3f, z = %.3f\n", uav_cur_pose.pose.position.x, uav_cur_pose.pose.position.y, uav_cur_pose.pose.position.z);
        pos.enabled = false;
      }
      pos.position[0] = uav_cur_pose.pose.position.x + target_pos_offset[0];
      pos.position[1] = uav_cur_pose.pose.position.y + target_pos_offset[1];
      pos.position[2] = uav_cur_pose.pose.position.z + target_pos_offset[2];

//      lcm_geometry_msgs::TwistStamped vel;
//      vel.timestamp = uav_cur_vel.header.stamp.toSec();
//      vel.timestamp = ros::Time::now().toSec();
//      vel.linear[0] = uav_cur_vel.twist.linear.x;
//      vel.linear[1] = uav_cur_vel.twist.linear.y;
//      vel.linear[2] = uav_cur_vel.twist.linear.z;
      /*
        lcm_geometry_msgs::PoseStamped pose;
        pose.timestamp = ros::Time::now().toSec();
        pose.position[0] = 0.0;
        pose.position[1] = 1.0;
        pose.position[2] = 2.0;
        pose.orientation[0] = 1.0;
        pose.orientation[1] = 0.0;
        pose.orientation[2] = 0.0;
        pose.orientation[3] = 0.0;

        lcm_geometry_msgs::TwistStamped vel;
        vel.timestamp = ros::Time::now().toSec();
        vel.linear[0] = 4.0;
        vel.linear[1] = 5.0;
        vel.linear[2] = 6.0;
      */
        lcm.publish("/lcm/" + target_uav_name + "/target_pose", &pos);
//        lcm.publish("/lcm" + uav_name + "/local_position/velocity_local", &vel);

        ctrl_loop.sleep();
    }
    return 0;
}