#include <iostream>
#include <fstream>
#include <sstream>
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <lcm/lcm-cpp.hpp>
#include <colors.h>
#include <lcm_geometry_msgs/PoseStamped.hpp>
#include <lcm_geometry_msgs/PositionStamped.hpp>
#include <lcm_geometry_msgs/TwistStamped.hpp>
#include <Filter/SmoothFilter.h>

#define PI 3.1415926
using namespace std;

geometry_msgs::PoseStamped uav_cur_pose;
geometry_msgs::TwistStamped uav_cur_vel;
int droneID = 0;
std::string self_uav_name = "kun0";
std::string neighbor_uav_name = "kun1";
std::string uav_vel_msg, uav_pose_msg;
int ctrl_rate = 30;
FILTER _smooth_pos_x(30);
FILTER _smooth_pos_y(30);
FILTER _smooth_pos_z(30);

double target_pos_offset[3] = {0, -1, 0}; //在kun0正常当前值上加上这个偏移量，得到kun1的目标值

void uav_pose_cb(const geometry_msgs::PoseStamped::ConstPtr& msg){
    uav_cur_pose = *msg;
}

void uav_vel_cb(const geometry_msgs::TwistStamped::ConstPtr& msg){
    uav_cur_vel = *msg;
}

// Main func
int main(int argc, char** argv){
    // ros init
    ros::init(argc, argv, "local_pose_pub", ros::init_options::AnonymousName);
    ros::NodeHandle nh;

    lcm::LCM lcm("udpm://239.255.76.67:7667?ttl=1");
//    lcm::LCM lcm("udpm://224.0.0.250:7667?ttl=255");
    if(!lcm.good())
        return 1;

    ros::Time last_request = ros::Time::now();
    ros::Subscriber uav_pose_sub = nh.subscribe<geometry_msgs::PoseStamped>("mavros/local_position/pose", 2, uav_pose_cb);
    std::string lcm_channel = "/lcm/" + neighbor_uav_name + "/target_pose";

    ros::Rate ctrl_loop(ctrl_rate);
    ROS_INFO("Subscriber: %s", uav_pose_sub.getTopic().c_str());
    ROS_INFO("LCM Publisher: %s", lcm_channel.c_str());
    
    //main ctrl loop
    while(ros::ok()) {
      ros::spinOnce();

      //smooth uav_cur_pose to publish a smooth pose
      _smooth_pos_x.filter_input(uav_cur_pose.pose.position.x, ros::Time::now().toSec());
      uav_cur_pose.pose.position.x = _smooth_pos_x.filter_output();
      _smooth_pos_y.filter_input(uav_cur_pose.pose.position.y, ros::Time::now().toSec());
      uav_cur_pose.pose.position.y = _smooth_pos_y.filter_output();
      _smooth_pos_z.filter_input(uav_cur_pose.pose.position.z, ros::Time::now().toSec());
      uav_cur_pose.pose.position.z = _smooth_pos_z.filter_output();

      lcm_geometry_msgs::PositionStamped pos;
      pos.timestamp = ros::Time::now().toSec();
      pos.name = "kun0";
      if(abs(uav_cur_pose.pose.position.x) > 1e-7 && abs(uav_cur_pose.pose.position.y) > 1e-7){
        pos.enabled = true;
        printf(GREEN "[Normal] uav_cur_pose: x = %.3f, y = %.3f, z = %.3f\n", uav_cur_pose.pose.position.x, uav_cur_pose.pose.position.y, uav_cur_pose.pose.position.z);
      }else{
        printf(RED "[Bad] uav_cur_pose: x = %.3f, y = %.3f, z = %.3f\n", uav_cur_pose.pose.position.x, uav_cur_pose.pose.position.y, uav_cur_pose.pose.position.z);
        pos.enabled = false;
      }
      pos.position[0] = uav_cur_pose.pose.position.x + target_pos_offset[0];
      pos.position[1] = uav_cur_pose.pose.position.y + target_pos_offset[1];
      pos.position[2] = uav_cur_pose.pose.position.z + target_pos_offset[2];

//      lcm_geometry_msgs::TwistStamped vel;
//      vel.timestamp = uav_cur_vel.header.stamp.toSec();
//      vel.timestamp = ros::Time::now().toSec();
//      vel.linear[0] = uav_cur_vel.twist.linear.x;
//      vel.linear[1] = uav_cur_vel.twist.linear.y;
//      vel.linear[2] = uav_cur_vel.twist.linear.z;
      /*
        lcm_geometry_msgs::PoseStamped pose;
        pose.timestamp = ros::Time::now().toSec();
        pose.position[0] = 0.0;
        pose.position[1] = 1.0;
        pose.position[2] = 2.0;
        pose.orientation[0] = 1.0;
        pose.orientation[1] = 0.0;
        pose.orientation[2] = 0.0;
        pose.orientation[3] = 0.0;

        lcm_geometry_msgs::TwistStamped vel;
        vel.timestamp = ros::Time::now().toSec();
        vel.linear[0] = 4.0;
        vel.linear[1] = 5.0;
        vel.linear[2] = 6.0;
      */

        lcm.publish(lcm_channel, &pos);
//        lcm.publish("/lcm" + uav_name + "/local_position/velocity_local", &vel);

        ctrl_loop.sleep();
    }
    return 0;
}