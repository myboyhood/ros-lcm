#include <iostream>
#include <fstream>
#include <sstream>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Time.h>
#include <lcm/lcm-cpp.hpp>
#include <colors.h>
#include <lcm_sensor_msgs/Imu.hpp>
#include <lcm_nav_msgs/Odometry.hpp>
#include <lcm_std_msgs/NameTime.hpp>

#define PI 3.1415926
using namespace std;

sensor_msgs::Imu imu_ros_msg;
nav_msgs::Odometry odom_ros_msg;
std_msgs::Time offb_time_ros_msg;
int ctrl_rate = 200;
ros::Publisher imu_pub;
ros::Publisher odom_pub;
ros::Publisher offb_time_pub;

class Handler 
{
    public:
        ~Handler() {}
        void lcm_imu_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan, const lcm_sensor_msgs::Imu* msg){
//            printf("Received message on channel \"%s\":\n", chan.c_str());
//            printf("  timestamp   = %.3f | local_timestamp = %.3f\n", msg->timestamp, ros::Time::now().toSec());
//            printf("  acc = (%.3f, %.3f, %.3f)\n", msg->linear_acceleration[0], msg->linear_acceleration[1], msg->linear_acceleration[2]);
            ROS_INFO_THROTTLE(5, GREEN "[Normal] imu time delay %.1f ms", 1000*(ros::Time::now().toSec() - msg->timestamp));

            imu_ros_msg.header.stamp = ros::Time().fromSec(msg->timestamp);
            imu_ros_msg.header.frame_id = "global";
            imu_ros_msg.linear_acceleration.x = msg->linear_acceleration[0];
            imu_ros_msg.linear_acceleration.y = msg->linear_acceleration[1];
            imu_ros_msg.linear_acceleration.z = msg->linear_acceleration[2];
            imu_ros_msg.angular_velocity.x = msg->angular_velocity[0];
            imu_ros_msg.angular_velocity.y = msg->angular_velocity[1];
            imu_ros_msg.angular_velocity.z = msg->angular_velocity[2];
            imu_ros_msg.orientation.w = msg->orientation.w;
            imu_ros_msg.orientation.x = msg->orientation.x;
            imu_ros_msg.orientation.y = msg->orientation.y;
            imu_ros_msg.orientation.z = msg->orientation.z;

            imu_pub.publish(imu_ros_msg);//直接转为ros消息发布
        }

        void lcm_odom_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan, const lcm_nav_msgs::Odometry* msg){
          ROS_INFO_THROTTLE(5, GREEN "[Normal] odom time delay %.1f ms", 1000*(ros::Time::now().toSec() - msg->timestamp));

          odom_ros_msg.header.stamp = ros::Time().fromSec(msg->timestamp);
          odom_ros_msg.header.frame_id = "global";
          odom_ros_msg.pose.pose.position.x = msg->position[0];
          odom_ros_msg.pose.pose.position.y = msg->position[1];
          odom_ros_msg.pose.pose.position.z = msg->position[2];
          odom_ros_msg.twist.twist.linear.x = msg->velocity[0];
          odom_ros_msg.twist.twist.linear.y = msg->velocity[1];
          odom_ros_msg.twist.twist.linear.z = msg->velocity[2];
          odom_ros_msg.pose.pose.orientation.w = msg->orientation.w;
          odom_ros_msg.pose.pose.orientation.x = msg->orientation.x;
          odom_ros_msg.pose.pose.orientation.y = msg->orientation.y;
          odom_ros_msg.pose.pose.orientation.z = msg->orientation.z;

          odom_pub.publish(odom_ros_msg);//直接转为ros消息发布
        }

        void lcm_offb_time_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan, const lcm_std_msgs::NameTime* msg){
          ROS_INFO_THROTTLE(5, GREEN "[Normal] offb_time time delay %.1f ms", 1000*(ros::Time::now().toSec() - msg->timestamp));
          offb_time_ros_msg.data = ros::Time().fromSec(msg->timestamp);
          offb_time_pub.publish(offb_time_ros_msg);//直接转为ros消息发布
        }
};

// Main func
int main(int argc, char** argv){
    
    // ros init
    ros::init(argc, argv, "ros_lcm_sub", ros::init_options::AnonymousName);
    ros::NodeHandle nh;

    lcm::LCM lcm("udpm://239.255.76.67:7667?ttl=1");

    Handler handlerObject;
    // Handler handlerObjectOdom;
    // Handler handlerObjectOffb;
    lcm.subscribe("/lcm/kun0/imu/data_filter", &Handler::lcm_imu_cb, &handlerObject);
    lcm.subscribe("/lcm/kun0/kun0_camera/odom/sample", &Handler::lcm_odom_cb, &handlerObject);
    lcm.subscribe("/lcm/kun0/offb_time", &Handler::lcm_offb_time_cb, &handlerObject);
//    ROS_INFO("LCM Subscriber: %s", lcm_channel.c_str());
    std::string uav_name = "/kun0";
    imu_pub = nh.advertise<sensor_msgs::Imu>(uav_name + "/imu/data_filter", 1);
    odom_pub = nh.advertise<nav_msgs::Odometry>(uav_name + "/kun0_camera/odom/sample", 1);
    offb_time_pub = nh.advertise<std_msgs::Time>(uav_name + "/offb_time", 1);

    ros::Rate ctrl_loop(ctrl_rate);
    
    //main ctrl loop
    while(ros::ok()) {
//      ROS_INFO_THROTTLE(1,"ros_lcm_sub is running, lcm.good(): %d", lcm.good());
      if (!lcm.good()) {
          printf("stop\n");
          break;
      }
      if (lcm.handle() != 0)
          break;
      ctrl_loop.sleep();
    }
    return 0;
}