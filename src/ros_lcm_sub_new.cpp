#include <iostream>
#include <fstream>
#include <sstream>
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <lcm/lcm-cpp.hpp>

#include <lcm_geometry_msgs/PoseStamped.hpp>
#include <lcm_geometry_msgs/TwistStamped.hpp>
#include <lcm_geometry_msgs/PositionStamped.hpp>

#define PI 3.1415926
using namespace std;

geometry_msgs::PoseStamped uav_cur_pose;
geometry_msgs::TwistStamped uav_cur_vel;
int ctrl_rate = 30;

class Handler 
{
    public:
        ~Handler() {}
        void lcm_position_cb(const lcm::ReceiveBuffer* rbuf, const std::string& chan, const lcm_geometry_msgs::PositionStamped* msg){
            printf("Received message on channel \"%s\":\n", chan.c_str());
            printf("  timestamp   = %.3f | local_timestamp = %.3f\n", msg->timestamp, ros::Time::now().toSec());
            printf("  position    = (%.3f, %.3f, %.3f)\n", msg->position[0], msg->position[1], msg->position[2]);
            uav_cur_pose.header.stamp = ros::Time::now();
            uav_cur_pose.header.frame_id = "global";
            uav_cur_pose.pose.position.x = msg->position[0];
            uav_cur_pose.pose.position.y = msg->position[1];
            uav_cur_pose.pose.position.z = msg->position[2];
        }
};

// Main func
int main(int argc, char** argv){
    
    // ros init
    ros::init(argc, argv, "ros_lcm_sub", ros::init_options::AnonymousName);
    ros::NodeHandle nh;

    lcm::LCM lcm("udpm://239.255.76.67:7667?ttl=1");

    Handler handlerObject;
    std::string lcm_channel = "/lcm/kun1/target_pose";
    lcm.subscribe(lcm_channel, &Handler::lcm_position_cb, &handlerObject);
    ROS_INFO("LCM Subscriber: %s", lcm_channel.c_str());
    std::string uav_name = "/kun0";
    ros::Publisher uav_pose_pub = nh.advertise<geometry_msgs::PoseStamped>(uav_name + "/mavros/local_position/pose", 1);
//    ros::Publisher uav_vel_pub = nh.advertise<geometry_msgs::TwistStamped>(uav_name + "/mavros/local_position/velocity_local", 1);

    ros::Rate ctrl_loop(ctrl_rate);
    
    //main ctrl loop
    while(ros::ok()) {
      ROS_INFO_THROTTLE(1,"ros_lcm_sub is running, lcm.good(): %d", lcm.good());
//        ros::spinOnce();
        if (!lcm.good()) {
            printf("stop\n");
            break;
        }
        if (lcm.handle() != 0)
            break;
        uav_pose_pub.publish(uav_cur_pose);
//        uav_vel_pub.publish(uav_cur_vel);
        ctrl_loop.sleep();
    }
    return 0;
}