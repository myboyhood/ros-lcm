#include <iostream>
#include <fstream>
#include <sstream>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Time.h>
#include <lcm/lcm-cpp.hpp>
#include <colors.h>
#include <lcm_sensor_msgs/Imu.hpp>
#include <lcm_nav_msgs/Odometry.hpp>
#include <lcm_std_msgs/NameTime.hpp>

#define PI 3.1415926
using namespace std;

sensor_msgs::Imu imu_ros_msg;
nav_msgs::Odometry odom_ros_msg;
std_msgs::Time offb_time_ros_msg;
int droneID = 0;
std::string uav_name = "/kun" + to_string(droneID);
int ctrl_rate = 30;

// tool func
template<typename T>
void readParam(ros::NodeHandle &nh, std::string param_name, T& loaded_param) {
    // template to read param from roslaunch
    const string& node_name = ros::this_node::getName();
    param_name = node_name + "/" + param_name;
    if (!nh.getParam(param_name, loaded_param)) 
        ROS_ERROR_STREAM("Failed to load " << param_name << ", use default value");
    else
        ROS_INFO_STREAM("Load " << param_name << " success");
}
void loadRosParams(ros::NodeHandle &nh) {
    readParam<int>(nh, "droneID", droneID);
    ROS_INFO_STREAM("drone ID = " << droneID);
    uav_name = "/kun" + to_string(droneID);
}

void imu_cb(const sensor_msgs::Imu::ConstPtr& msg){
  imu_ros_msg = *msg;
}

void odom_cb(const nav_msgs::Odometry::ConstPtr& msg){
  odom_ros_msg = *msg;
}

void offb_time_cb(const std_msgs::Time::ConstPtr& msg){
  offb_time_ros_msg = *msg;
}

// Main func
int main(int argc, char** argv){
    // ros init
    ros::init(argc, argv, "ros_lcm_pub", ros::init_options::AnonymousName);
    ros::NodeHandle nh;

    // load param
    loadRosParams(nh);

    lcm::LCM lcm("udpm://239.255.76.67:7667?ttl=1");
    if(!lcm.good())
        return 1;

    ros::Time last_request = ros::Time::now();
    ros::Subscriber imu_sub = nh.subscribe<sensor_msgs::Imu>(uav_name + "/imu/data_filter", 2, imu_cb);
    ros::Subscriber odom_sub = nh.subscribe<nav_msgs::Odometry>("/kun0/kun0_camera/odom/sample", 2, odom_cb);
    ros::Subscriber offb_time_sub = nh.subscribe<std_msgs::Time>("/kun0/offb_time", 2, offb_time_cb);

    ros::Rate ctrl_loop(ctrl_rate);
    
    //main ctrl loop
    while(ros::ok()) {
      ros::spinOnce();

      lcm_sensor_msgs::Imu imu_lcm_msg;
      imu_lcm_msg.timestamp = ros::Time::now().toSec();
      imu_lcm_msg.name = "kun0";

      imu_lcm_msg.linear_acceleration[0] = imu_ros_msg.linear_acceleration.x;
      imu_lcm_msg.linear_acceleration[1] = imu_ros_msg.linear_acceleration.y;
      imu_lcm_msg.linear_acceleration[2] = imu_ros_msg.linear_acceleration.z;
      imu_lcm_msg.angular_velocity[0] = imu_ros_msg.angular_velocity.x;
      imu_lcm_msg.angular_velocity[1] = imu_ros_msg.angular_velocity.y;
      imu_lcm_msg.angular_velocity[2] = imu_ros_msg.angular_velocity.z;
      imu_lcm_msg.orientation.w = imu_ros_msg.orientation.w;
      imu_lcm_msg.orientation.x = imu_ros_msg.orientation.x;
      imu_lcm_msg.orientation.y = imu_ros_msg.orientation.y;
      imu_lcm_msg.orientation.z = imu_ros_msg.orientation.z;

      ROS_INFO_THROTTLE(5, GREEN "[Normal] imu acc: x = %.3f, y = %.3f, z = %.3f", imu_lcm_msg.linear_acceleration[0], imu_lcm_msg.linear_acceleration[1], imu_lcm_msg.linear_acceleration[2]);
      lcm.publish("/lcm/kun0/imu/data_filter", &imu_lcm_msg);


      lcm_nav_msgs::Odometry odom_lcm_msg;
      odom_lcm_msg.timestamp = ros::Time::now().toSec();
      odom_lcm_msg.name = "kun0";

      odom_lcm_msg.position[0] = odom_ros_msg.pose.pose.position.x;
      odom_lcm_msg.position[1] = odom_ros_msg.pose.pose.position.y;
      odom_lcm_msg.position[2] = odom_ros_msg.pose.pose.position.z;
      odom_lcm_msg.velocity[0] = odom_ros_msg.twist.twist.linear.x;
      odom_lcm_msg.velocity[1] = odom_ros_msg.twist.twist.linear.y;
      odom_lcm_msg.velocity[2] = odom_ros_msg.twist.twist.linear.z;
      odom_lcm_msg.orientation.w = odom_ros_msg.pose.pose.orientation.w;
      odom_lcm_msg.orientation.x = odom_ros_msg.pose.pose.orientation.x;
      odom_lcm_msg.orientation.y = odom_ros_msg.pose.pose.orientation.y;
      odom_lcm_msg.orientation.z = odom_ros_msg.pose.pose.orientation.z;

      ROS_INFO_THROTTLE(5, GREEN "[Normal] odom pos: x = %.3f, y = %.3f, z = %.3f", odom_lcm_msg.position[0], odom_lcm_msg.position[1], odom_lcm_msg.position[2]);
      lcm.publish("/lcm/kun0/kun0_camera/odom/sample", &odom_lcm_msg);


      lcm_std_msgs::NameTime offb_time_lcm_msg;
      offb_time_lcm_msg.name = "kun0";
      offb_time_lcm_msg.timestamp = offb_time_ros_msg.data.toSec();
      ROS_INFO_THROTTLE(5, GREEN "[Normal] offb_time = %.3f", offb_time_lcm_msg.timestamp);
      lcm.publish("/lcm/kun0/offb_time", &offb_time_lcm_msg);

      ctrl_loop.sleep();
    }
    return 0;
}