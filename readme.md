### lcm通讯

- 安装lcm

  - ```shell
    git clone -b v1.4.0 https://github.com/lcm-proj/lcm
    ```

  - [安装依赖](https://lcm-proj.github.io/lcm/content/build-instructions.html#build-instructions)

    ```shell
    sudo apt-get install build-essential cmake libglib2.0-dev default-jdk libjchart2d-java doxygen liblua5.3-dev lua5.3 python3-dev
    ```

    ```shell
    sudo apt-get install build-essential cmake libglib2.0-dev openjdk-8-jdk libjchart2d-java doxygen liblua5.3-dev lua5.3 python3-dev
    ```

    

  - 编译

    ```shell
    cd lcm
    mkdir build
    cd build
    cmake ..
    make
    export LCM_INSTALL_DIR=/usr/local/lib
    sudo make install
    sudo sh -c "echo $LCM_INSTALL_DIR > /etc/ld.so.conf.d/lcm.conf"
    sudo ldconfig
    export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$LCM_INSTALL_DIR/pkgconfig
    ```

  - 测试

    ```shell
    lcm-spy
    ```

- lcm通讯教程

  - 编写消息文件

    ```c++
    package exlcm;
    struct example_t
    {
        double timestamp;
        double  position[3];
        double  orientation[4];
        int32_t num_ranges;
        int16_t ranges[num_ranges];
        string  name;
        boolean enable;
    }
    ```

  - 编译消息文件

    ```sh
    lcm-gen -x example_t.lcm
    ```

  - 编写lcm_send.cpp

    ```c++
    #include <lcm/lcm-cpp.hpp>
    #include "exlcm/example_t.hpp"
    
    int main(int argc, char ** argv)
    {
        lcm::LCM lcm("udpm://239.255.76.67:7667?ttl=1");
        if(!lcm.good())
            return 1;
        exlcm::example_t my_data;
        my_data.timestamp = 0;
        my_data.position[0] = 1;
        my_data.position[1] = 2;
        my_data.position[2] = 3;
        my_data.orientation[0] = 1;
        my_data.orientation[1] = 0;
        my_data.orientation[2] = 0;
        my_data.orientation[3] = 0;
        my_data.num_ranges = 15;
        my_data.ranges.resize(my_data.num_ranges);
        for(int i = 0; i < my_data.num_ranges; i++)
            my_data.ranges[i] = i;
        my_data.name = "example string from computer1";
        my_data.enable = true;
        lcm.publish("EXAMPLE", &my_data);
        return 0;
    }
    ```

  - 编写lcm_receive.cpp

    ```c++
    #include <stdio.h>
    #include <lcm/lcm-cpp.hpp>
    #include "exlcm/example_t.hpp"
    class Handler 
    {
        public:
            ~Handler() {}
            void handleMessage(const lcm::ReceiveBuffer* rbuf,
                    const std::string& chan, 
                    const exlcm::example_t* msg)
            {
                int i;
                printf("Received message on channel \"%s\":\n", chan.c_str());
                printf("  timestamp   = %lld\n", (long long)msg->timestamp);
                printf("  position    = (%f, %f, %f)\n",
                        msg->position[0], msg->position[1], msg->position[2]);
                printf("  orientation = (%f, %f, %f, %f)\n",
                        msg->orientation[0], msg->orientation[1], 
                        msg->orientation[2], msg->orientation[3]);
                printf("  ranges:");
                for(i = 0; i < msg->num_ranges; i++)
                    printf(" %d", msg->ranges[i]);
                printf("\n");
                printf("  name        = '%s'\n", msg->name.c_str());
                printf("  enable     = %d\n", msg->enable);
            }
    };
    int main(int argc, char** argv)
    {
        lcm::LCM lcm("udpm://239.255.76.67:7667?ttl=1");
        if(!lcm.good())
            return 1;
        Handler handlerObject;
        lcm.subscribe("EXAMPLE", &Handler::handleMessage, &handlerObject);
        while(0 == lcm.handle());
        return 0;
    }
    ```

  - 在CMakelists.txt中编译项

    ```cmake
    add_executable(lcm_send src/lcm_send.cpp)
    target_link_libraries(lcm_send lcm)
    
    add_executable(lcm_receive src/lcm_receive.cpp)
    target_link_libraries(lcm_receive lcm)
    ```

- ros-lcm通讯教程

  - 编写ros_lcm_pub.cpp

    ```c++
    #include <iostream>
    #include <fstream>
    #include <sstream>
    #include <ros/ros.h>
    #include <geometry_msgs/PoseStamped.h>
    #include <geometry_msgs/TwistStamped.h>
    #include <lcm/lcm-cpp.hpp>
    
    #include "exlcm/example_t.hpp"
    
    #define PI 3.1415926
    using namespace std;
    
    geometry_msgs::PoseStamped uav_cur_pose;
    geometry_msgs::TwistStamped uav_cur_vel;
    int droneID = 0;
    std::string uav_name = "/uav" + to_string(droneID);
    int ctrl_rate = 30;
    
    void uav_pose_cb(const geometry_msgs::PoseStamped::ConstPtr& msg){
        uav_cur_pose = *msg;
    }
    
    void uav_vel_cb(const geometry_msgs::TwistStamped::ConstPtr& msg){
        uav_cur_vel = *msg;
    }
    
    // Main func
    int main(int argc, char** argv){
        
        // ros init
        ros::init(argc, argv, "ros_lcm", ros::init_options::AnonymousName);
        ros::NodeHandle nh;
    
        lcm::LCM lcm("udpm://239.255.76.67:7667?ttl=1");
        if(!lcm.good())
            return 1;
    
        ros::Time last_request = ros::Time::now();
        
        ros::Subscriber uav_pose_sub = nh.subscribe<geometry_msgs::PoseStamped>
                (uav_name + "/mavros/local_position/pose", 2, uav_pose_cb);
        ros::Subscriber uav_vel_sub = nh.subscribe<geometry_msgs::TwistStamped>
                (uav_name + "/mavros/local_position/velocity_local", 1, uav_vel_cb);
        
        ros::Rate ctrl_loop(ctrl_rate);
        
        //main ctrl loop
        while(ros::ok()) {
    
            exlcm::example_t my_data;
            my_data.timestamp = ros::Time::now().toSec();
            my_data.position[0] = uav_cur_pose.pose.position.x;
            my_data.position[1] = uav_cur_pose.pose.position.y;
            my_data.position[2] = uav_cur_pose.pose.position.z;
            my_data.orientation[0] = uav_cur_pose.pose.orientation.w;
            my_data.orientation[1] = uav_cur_pose.pose.orientation.x;
            my_data.orientation[2] = uav_cur_pose.pose.orientation.y;
            my_data.orientation[3] = uav_cur_pose.pose.orientation.z;
            my_data.num_ranges = 15;
            my_data.ranges.resize(my_data.num_ranges);
            for(int i = 0; i < my_data.num_ranges; i++)
                my_data.ranges[i] = i;
            my_data.name = "example string from computer1";
            my_data.enable = true;
            lcm.publish("EXAMPLE", &my_data);
            
            ros::spinOnce();
            // spinner.spin();
            ctrl_loop.sleep();
        }
        return 0;
    }
    ```

  - 编写ros_lcm_sub.cpp

    ```c++
    #include <iostream>
    #include <fstream>
    #include <sstream>
    #include <ros/ros.h>
    #include <geometry_msgs/PoseStamped.h>
    #include <geometry_msgs/TwistStamped.h>
    #include <lcm/lcm-cpp.hpp>
    #include "exlcm/example_t.hpp"
    
    #define PI 3.1415926
    using namespace std;
    
    geometry_msgs::PoseStamped uav_cur_pose;
    geometry_msgs::TwistStamped uav_cur_vel;
    int droneID = 0;
    std::string uav_name = "/uav" + to_string(droneID);
    int ctrl_rate = 30;
    
    class Handler 
    {
        public:
            ~Handler() {}
            void handleMessage(const lcm::ReceiveBuffer* rbuf,
                    const std::string& chan, 
                    const exlcm::example_t* msg)
            {
                int i;
                printf("Received message on channel \"%s\":\n", chan.c_str());
                printf("  timestamp   = %lld\n", (long long)msg->timestamp);
                printf("  position    = (%f, %f, %f)\n",
                        msg->position[0], msg->position[1], msg->position[2]);
                printf("  orientation = (%f, %f, %f, %f)\n",
                        msg->orientation[0], msg->orientation[1], 
                        msg->orientation[2], msg->orientation[3]);
                printf("  ranges:");
                for(i = 0; i < msg->num_ranges; i++)
                    printf(" %d", msg->ranges[i]);
                printf("\n");
                printf("  name       = '%s'\n", msg->name.c_str());
                printf("  enable     = %d\n", msg->enable);
            }
    };
    
    // Main func
    int main(int argc, char** argv){
        
        // ros init
        ros::init(argc, argv, "ros_lcm", ros::init_options::AnonymousName);
        ros::NodeHandle nh;
    
        lcm::LCM lcm("udpm://239.255.76.67:7667?ttl=1");
    
        Handler handlerObject;
        lcm.subscribe("EXAMPLE", &Handler::handleMessage, &handlerObject);
    
        ros::Rate ctrl_loop(ctrl_rate);
        
        //main ctrl loop
        while(ros::ok()) {
    
            ros::spinOnce();
            if (!lcm.good()) {
                printf("stop\n");
                break;
            }
            if (lcm.handle() != 0)
                break;
            ctrl_loop.sleep();
        }
        return 0;
    }
    ```

  - 在CMakelists.txt中编译项

    ```cmake
    add_executable(ros_lcm_pub src/ros_lcm_pub.cpp)
    add_dependencies(ros_lcm_pub ${PROJECT_NAME}_generate_messages_cpp)
    target_link_libraries(ros_lcm_pub lcm filter1 PIDlib ${catkin_LIBRARIES})
    
    add_executable(ros_lcm_sub src/ros_lcm_sub.cpp)
    add_dependencies(ros_lcm_sub ${PROJECT_NAME}_generate_messages_cpp)
    target_link_libraries(ros_lcm_sub lcm filter1 PIDlib ${catkin_LIBRARIES}
    ```